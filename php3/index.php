<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

echo "<h4>output akhir</h4>";

$sheep = new animal("shaun");

echo "Nama hewan: ". $sheep->name; // "shaun"
echo "<br>";
echo "Jumlah kaki: ". $sheep->legs; // 4
echo "<br>";
echo "Apakah berdarah dingin? ". $sheep->cold_blooded; // "no"
echo "<br> <br>";

$kodok = new frog("buduk");
echo "Nama hewan: ". $kodok->name; // "shaun"
echo "<br>";
echo "Jumlah kaki: ". $kodok->legs; // 4
echo "<br>";
echo "Apakah berdarah dingin? ". $kodok->cold_blooded; // "no"
echo "<br>";
echo "Bunyi saat melompat: ".$kodok->jump() ; // "hop hop"
echo "<br> <br>"; 

$sungokong = new ape("kera sakti");
echo "Nama hewan: ". $sungokong->name; // "shaun"
echo "<br>";
echo "Jumlah kaki: ". $sungokong->legs; // 4
echo "<br>";
echo "Apakah berdarah dingin? ". $sungokong->cold_blooded; // "no"
echo "<br>";
echo "Bunyi teriakan: ".$sungokong->yell(); // "Auooo"
echo "<br> <br>";

?>