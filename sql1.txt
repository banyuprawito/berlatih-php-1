1. Buat database
create database myshop;

2 buat table

 create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(12),
    -> stock int(5),
    -> category_id int(8) not null,
    -> foreign key(category_id) references categories(id)
    -> );

3. Memasukkan data pada table

insert into categories(name) values(
    -> "gadget"), ("cloth"), ("men"), ("women"), ("branded");

insert into users(name, email, password) values
    -> ("John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");

insert into items(name, description, price, stock, category_id) values
    -> ("Sumsang b50", "hape keren merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil data
	a.  select id, name, email from users;

	b. select * from items where price >1000000;
	   select * from items where name like "%sang%" ;


	c. select items.name, items.description, items.price, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;


5. Mengubah data

update items set price = 2500000 where id=1;
